.. amarium documentation master file, created by
   sphinx-quickstart on Tue Sep 19 14:53:45 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to amarium's documentation!
==================================

You made it to the Amarium. Amarium is a library that handles tested and secure file 
operations for developers. If you need to work with local files reliably, amarium is the way! 

Make sure to check out reference for information about our functions. The naming is quite intuitive 
and should be clear by just looking at the inputs and outputs. 


If you have further questions, please let us know. 
The project lives from feedback, so feel free to give it. 

Package Tree
============

.. toctree::
   
   :maxdepth: 3
   :caption: Contents

   src.amarium



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
