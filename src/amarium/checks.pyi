from typing import Any, List

def check_header_and_data(data: List[Any], header: List[Any]) -> None: ...
def check_make_file_name_suffix(file_name: str, suffix: str = ...) -> str: ...
def check_make_dir(dir_name: str) -> None: ...
