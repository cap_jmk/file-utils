# Copyright Julian M. Kleber 

echo "local-ci  Copyright (C) 2022 Julian M. Kleber This program comes with ABSOLUTELY NO WARRANTY; for details type 'show w'. This is free software, and you are welcome to redistribute it under certain conditions; type 'show c' for details."
pip freeze > requirements.txt
black src/amarium/
find src/amarium/ -type f -name '*.txt' -exec sed --in-place 's/[[:space:]]\+$//' {} \+ #sanitize trailing whitespace
autopep8 --in-place --recursive --max-line-length 100 src/amarium/ -j -1
python -m flake8 src/amarium/ --count --select=E9,F63,F7,F82 --show-source --statistics
mypy --strict src/amarium/
python -m pylint -f parseable src/amarium/*.py 
python3 -m pytest tests/